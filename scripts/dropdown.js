document.getElementById('avatar').addEventListener('click', function () {
    let dropdown = document.getElementById('dropdown-user');

    if (dropdown.style.display === 'block') {
        dropdown.style.display = 'none';
    } else {
        dropdown.style.display = 'block';
    }
})