const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');

gulp.task('minify-css', function() {
    return gulp.src('./styles/*.css')
        .pipe(cleanCss())
        .pipe(concat('min.css'))
        .pipe(gulp.dest('./styles/'))
});